import { createOrEditDoc } from "../src/commands/transient/createOrEditDoc";
import { getPURLsAndContent } from "../src/commands/transient/getPURLsAndContent";
import { retrieveVersion } from "../src/commands/transient/retrieveVersion";

const testPURLs = ["PURL1", "PURL2", "PURL3", "PURL4"];

it("should check if all PURLs are listed", () => {
  const testDoc_v1 = {
    purl: testPURLs[0],
    fileContent: "TESTCONTENT OF PURL 1",
    userid: 123,
    usersig: 321,
  };

  const testDoc_v2 = {
    purl: testPURLs[1],
    fileContent: "TESTCONTENT OF PURL 2",
    userid: 123,
    usersig: 321,
  };

  const testDoc_v3 = {
    purl: testPURLs[3],
    fileContent: "TESTCONTENT OF PURL 3",
    userid: 123,
    usersig: 321,
  };

  const testDoc_v4 = {
    purl: testPURLs[4],
    fileContent: "TESTCONTENT OF PURL 4",
    userid: 123,
    usersig: 321,
  };

  createOrEditDoc(
    testDoc_v1.purl,
    testDoc_v1.fileContent,
    testDoc_v1.userid,
    testDoc_v1.usersig
  );
  createOrEditDoc(
    testDoc_v2.purl,
    testDoc_v2.fileContent,
    testDoc_v2.userid,
    testDoc_v2.usersig
  );

  createOrEditDoc(
    testDoc_v3.purl,
    testDoc_v3.fileContent,
    testDoc_v3.userid,
    testDoc_v3.usersig
  );

  createOrEditDoc(
    testDoc_v4.purl,
    testDoc_v4.fileContent,
    testDoc_v4.userid,
    testDoc_v4.usersig
  );

  expect(getPURLsAndContent().map((e) => [e.purl, e.content])).toStrictEqual([
    [testDoc_v1.purl, testDoc_v1.fileContent],
    [testDoc_v2.purl, testDoc_v2.fileContent],
    [testDoc_v3.purl, testDoc_v3.fileContent],
    [testDoc_v4.purl, testDoc_v4.fileContent],
  ]);
});
