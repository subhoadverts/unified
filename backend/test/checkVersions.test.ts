import { createOrEditDoc } from "../src/commands/transient/createOrEditDoc";
import { retrieveVersion } from "../src/commands/transient/retrieveVersion";

const testPURL = "TestPURL";

it("should verify the contents of all versions of an edited Doc", () => {
  const testDoc_v1 = {
    purl: testPURL,
    fileContent: "TESTCONTENT OF VERSION 1",
    userid: 123,
    usersig: 321,
  };

  const testDoc_v2 = {
    purl: testPURL,
    fileContent: "TESTCONTENT OF VERSION 2",
    userid: 123,
    usersig: 321,
  };

  createOrEditDoc(
    testDoc_v1.purl,
    testDoc_v1.fileContent,
    testDoc_v1.userid,
    testDoc_v1.usersig
  );
  createOrEditDoc(
    testDoc_v2.purl,
    testDoc_v2.fileContent,
    testDoc_v2.userid,
    testDoc_v2.usersig
  );
  expect(retrieveVersion(testPURL, 1).fileContent).toBe(testDoc_v1.fileContent);
  expect(retrieveVersion(testPURL, 2).fileContent).toBe(testDoc_v2.fileContent);
});
