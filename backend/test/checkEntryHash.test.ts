import Base64 from "crypto-js/enc-base64";
import { SHA256 } from "crypto-js";
import { createOrEditDoc } from "../src/commands/transient/createOrEditDoc";
import { getProvenance } from "../src/commands/transient/getProvenance";
import { Doc } from "../src/models/transient/doc";

beforeAll(() => (Date.now = jest.fn(() => +new Date("2017-01-01"))));
afterAll(() => jest.clearAllMocks());

it("should verify the hash in block", () => {
  const testDoc = {
    purl: "testPURL",
    fileContent: "TESTCONTENT OF VERSION 1",
    userid: 1001,
    usersig: 1001,
    version: 1,
  };

  createOrEditDoc(
    testDoc.purl,
    testDoc.fileContent,
    testDoc.userid,
    testDoc.usersig
  );

  const diff = Doc.getBlankDoc().calculateDiff(testDoc.fileContent);
  let strToHash = "";
  diff.diffMap.forEach(
    (val, indx) =>
      (strToHash = strToHash.concat(val.toString() + indx.toString()))
  );
  strToHash = strToHash.concat(testDoc.userid.toString());
  strToHash = strToHash.concat(testDoc.purl);
  strToHash = strToHash.concat(testDoc.version.toString());
  strToHash = strToHash.concat(testDoc.usersig.toString());
  strToHash = strToHash.concat(Date.now().toString());

  const expectedHash = Base64.stringify(SHA256(strToHash));

  expect(
    getProvenance(testDoc.purl, 100).details.filter(
      (e) => e.version == testDoc.version
    )[0].hash
  ).toBe(expectedHash);
});
