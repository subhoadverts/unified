import { createOrEditDoc } from "../src/commands/transient/createOrEditDoc";
import { getProvenance } from "../src/commands/transient/getProvenance";

const testPURL = "TestPURL";

it("should verify the signatures in Provenance", () => {
  const testDoc_v1 = {
    purl: testPURL,
    fileContent: "TESTCONTENT OF VERSION 1",
    userid: 1001,
    usersig: 1001,
  };

  const testDoc_v2 = {
    purl: testPURL,
    fileContent: "TESTCONTENT OF VERSION 2",
    userid: 2002,
    usersig: 2002,
  };

  createOrEditDoc(
    testDoc_v1.purl,
    testDoc_v1.fileContent,
    testDoc_v1.userid,
    testDoc_v1.usersig
  );
  createOrEditDoc(
    testDoc_v2.purl,
    testDoc_v2.fileContent,
    testDoc_v2.userid,
    testDoc_v2.usersig
  );

  expect(
    getProvenance(testPURL, 3003).details.map((e) => [e.version, e.userId])
  ).toStrictEqual([
    [1, 1001],
    [2, 2002],
  ]);
});
