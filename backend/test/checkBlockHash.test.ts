import Base64 from "crypto-js/enc-base64";
import { SHA256 } from "crypto-js";
import { createOrEditDoc } from "../src/commands/transient/createOrEditDoc";
import { Doc } from "../src/models/transient/doc";
import { visualizeBlockchain } from "../src/commands/transient/visualizeBlockchain";

beforeAll(() => (Date.now = jest.fn(() => +new Date("2017-01-01"))));
afterAll(() => jest.clearAllMocks());

it("should verify the hash in entry", () => {
  const testDoc = {
    purl: "testPURL",
    fileContent: "TESTCONTENT OF VERSION 1",
    userid: 1001,
    usersig: 1001,
    version: 1,
  };

  createOrEditDoc(
    testDoc.purl,
    testDoc.fileContent,
    testDoc.userid,
    testDoc.usersig
  );

  const diff = Doc.getBlankDoc().calculateDiff(testDoc.fileContent);
  let strToHash = "";
  diff.diffMap.forEach(
    (val, indx) =>
      (strToHash = strToHash.concat(val.toString() + indx.toString()))
  );
  strToHash = strToHash.concat(testDoc.userid.toString());
  strToHash = strToHash.concat(testDoc.purl);
  strToHash = strToHash.concat(testDoc.version.toString());
  strToHash = strToHash.concat(testDoc.usersig.toString());
  strToHash = strToHash.concat(Date.now().toString());

  const expectedEntryHash = Base64.stringify(SHA256(strToHash));

  strToHash = expectedEntryHash;
  strToHash = strToHash.concat("1");
  strToHash = strToHash.concat(testDoc.userid.toString());
  strToHash = strToHash.concat(Date.now().toString());

  const expectedBlockHash = Base64.stringify(SHA256(strToHash));

  expect(
    visualizeBlockchain().filter((block) => block.blockId == 1)[0].blockHash
  ).toBe(expectedBlockHash);
});
