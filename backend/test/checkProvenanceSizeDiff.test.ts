import { createOrEditDoc } from "../src/commands/transient/createOrEditDoc";
import { getProvenance } from "../src/commands/transient/getProvenance";

const testPURL = "TestPURL";

it("should verify the size difference reported by Provenance", () => {
  const testDoc_v1 = {
    purl: testPURL,
    fileContent: "Checking size now",
    userid: 1001,
    usersig: 1001,
  };

  const testDoc_v2 = {
    purl: testPURL,
    fileContent: "Size goes down",
    userid: 1001,
    usersig: 1001,
  };

  const testDoc_v3 = {
    purl: testPURL,
    fileContent: "",
    userid: 1001,
    usersig: 1001,
  };

  const testDoc_v4 = {
    purl: testPURL,
    fileContent: "Size goes from zero to up",
    userid: 1001,
    usersig: 1001,
  };

  const sizeDiff_v1 = testDoc_v1.fileContent.length;
  const sizeDiff_v2 =
    testDoc_v2.fileContent.length - testDoc_v1.fileContent.length;
  const sizeDiff_v3 =
    testDoc_v3.fileContent.length - testDoc_v2.fileContent.length;
  const sizeDiff_v4 =
    testDoc_v4.fileContent.length - testDoc_v3.fileContent.length;

  createOrEditDoc(
    testDoc_v1.purl,
    testDoc_v1.fileContent,
    testDoc_v1.userid,
    testDoc_v1.usersig
  );
  createOrEditDoc(
    testDoc_v2.purl,
    testDoc_v2.fileContent,
    testDoc_v2.userid,
    testDoc_v2.usersig
  );
  createOrEditDoc(
    testDoc_v3.purl,
    testDoc_v3.fileContent,
    testDoc_v3.userid,
    testDoc_v3.usersig
  );
  createOrEditDoc(
    testDoc_v4.purl,
    testDoc_v4.fileContent,
    testDoc_v4.userid,
    testDoc_v4.usersig
  );

  expect(
    getProvenance(testPURL, 100).details.map((e) => [e.version, e.sizeDiff])
  ).toStrictEqual([
    [1, sizeDiff_v1],
    [2, sizeDiff_v2],
    [3, sizeDiff_v3],
    [4, sizeDiff_v4],
  ]);
});
