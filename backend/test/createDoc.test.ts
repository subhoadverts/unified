import { createOrEditDoc } from "../src/commands/transient/createOrEditDoc";
import { getDoc } from "../src/commands/transient/getDoc";

it("should verify the content of new Doc", () => {
  const testDoc = {
    purl: "TestPURL",
    fileContent: "TestContent",
    userid: 123,
    usersig: 321,
  };
  createOrEditDoc(
    testDoc.purl,
    testDoc.fileContent,
    testDoc.userid,
    testDoc.usersig
  );
  expect(getDoc(testDoc.purl).fileContent).toBe(testDoc.fileContent);
});
