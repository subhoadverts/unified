import { createOrEditDoc } from "../src/commands/transient/createOrEditDoc";
import { getDoc } from "../src/commands/transient/getDoc";

const testPURL = "TestPURL";

it("should verify if editing a document updates the content", () => {
  const testDoc_v1 = {
    purl: testPURL,
    fileContent: "TESTCONTENT OF VERSION 1",
    userid: 123,
    usersig: 321,
  };

  createOrEditDoc(
    testDoc_v1.purl,
    testDoc_v1.fileContent,
    testDoc_v1.userid,
    testDoc_v1.usersig
  );

  const testDoc_v2 = {
    purl: testPURL,
    fileContent: "TESTCONTENT OF VERSION 2",
    userid: 123,
    usersig: 321,
  };

  expect(getDoc(testPURL).fileContent).toBe(testDoc_v1.fileContent);
  createOrEditDoc(
    testDoc_v2.purl,
    testDoc_v2.fileContent,
    testDoc_v2.userid,
    testDoc_v2.usersig
  );

  expect(getDoc(testPURL).fileContent).toBe(testDoc_v2.fileContent);
});
