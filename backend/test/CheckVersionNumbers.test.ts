import { createOrEditDoc } from "../src/commands/transient/createOrEditDoc";
import { getVersionsForDoc } from "../src/commands/transient/getVersionsForDoc";

const testPURL = "Hello";

it("should check if all the versions are listed for a doc", () => {
  const testDoc_v1 = {
    purl: testPURL,
    fileContent: "TESTCONTENT FOR VERSION 1",
    userid: 123,
    usersig: 321,
  };

  const testDoc_v2 = {
    purl: testPURL,
    fileContent: "TESTCONTENT FOR VERSION 2",
    userid: 123,
    usersig: 321,
  };

  const testDoc_v3 = {
    purl: testPURL,
    fileContent: "TESTCONTENT OF VERSION 3",
    userid: 123,
    usersig: 321,
  };

  const testDoc_v4 = {
    purl: testPURL,
    fileContent: "TESTCONTENT OF VERSION 4",
    userid: 123,
    usersig: 321,
  };

  createOrEditDoc(
    testDoc_v1.purl,
    testDoc_v1.fileContent,
    testDoc_v1.userid,
    testDoc_v1.usersig
  );
  createOrEditDoc(
    testDoc_v2.purl,
    testDoc_v2.fileContent,
    testDoc_v2.userid,
    testDoc_v2.usersig
  );

  createOrEditDoc(
    testDoc_v3.purl,
    testDoc_v3.fileContent,
    testDoc_v3.userid,
    testDoc_v3.usersig
  );

  createOrEditDoc(
    testDoc_v4.purl,
    testDoc_v4.fileContent,
    testDoc_v4.userid,
    testDoc_v4.usersig
  );

  expect(getVersionsForDoc(testPURL)).toEqual([1, 2, 3, 4]);
});
