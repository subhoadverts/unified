import { Blockchain } from "../../models/transient/blockchain";
import { DocPool } from "../../models/transient/docPool";
import { Entry } from "../../models/transient/entry";

const listAllPURLs = (): Array<string> => {
  return DocPool.getInstance().getDocPURLs();
};

export { listAllPURLs };
