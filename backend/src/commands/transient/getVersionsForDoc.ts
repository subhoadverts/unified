import { Blockchain } from "../../models/transient/blockchain";
import { Differential } from "../../models/transient/differential";
import { Entry } from "../../models/transient/entry";
import { Provenance } from "../../models/transient/provenance";

const getVersionsForDoc = (purl: String): Array<number> => {
  return Blockchain.getInstance()
    .blocks.reduce<Entry[]>(
      (accum, current) => (accum = accum.concat(current.entries)),
      []
    )
    .filter((entry) => entry.linkedFilePURL == purl)
    .map((entry) => entry.versionNo)
    .sort();
};

export { getVersionsForDoc };
