import { Blockchain } from "../../models/transient/blockchain";
import { Differential } from "../../models/transient/differential";
import { Doc } from "../../models/transient/doc";
import { DocPool } from "../../models/transient/docPool";
import { Entry } from "../../models/transient/entry";

const retrieveVersion = (purl: string, version: number): Doc => {
  const relatedDifferentials = Blockchain.getInstance()
    .blocks.reduce(
      (accum, curr) => (accum = accum.concat(curr.entries)),
      new Array<Entry>()
    )
    .filter((e) => e.linkedFilePURL == purl && e.versionNo <= version)
    .sort((e) => e.timestamp)
    .map((e) => e.differential);

  let builtDoc = new Doc(purl, "");

  for (const diff of relatedDifferentials) {
    builtDoc = builtDoc.applyDiff(diff);
  }

  return builtDoc;
};

export { retrieveVersion };
