import { Blockchain } from "../../models/transient/blockchain";
import { Doc } from "../../models/transient/doc";
import { DocPool } from "../../models/transient/docPool";
import { Entry } from "../../models/transient/entry";

const filterEntries = (
  unfilteredEntries: Array<Entry>,
  userSig: number
): Array<Entry> => {
  const filterConditions = (x: Entry): boolean =>
    x.userSig == userSig && x.timestamp <= Date.now();

  return unfilteredEntries.filter(filterConditions);
};

const addBlock = (entries: Array<Entry>, userId: number) => {
  Blockchain.getInstance().addBlock(entries, userId);
};

const createOrEditDoc = (
  purl: string,
  fileContent: string,
  userId: number,
  userSig: number
):
  | { DATATYPE: "CREATEDDOC" | "UPDATEDDOC"; PURL: string; CONTENT: string }
  | false => {
  if (!DocPool.getInstance().pURLExists(purl)) {
    const diff = Doc.getBlankDoc().calculateDiff(fileContent);
    const newEntry = new Entry(userId, 1, diff, purl, userSig);
    if (filterEntries(new Array<Entry>(newEntry), userSig).length > 0) {
      addBlock(new Array<Entry>(newEntry), userId);
      DocPool.getInstance().addDoc(purl, fileContent);
      return { DATATYPE: "CREATEDDOC", PURL: purl, CONTENT: fileContent };
    }
  } else {
    const currFile = DocPool.getInstance().getDocByPURL(purl);
    const diff = currFile.calculateDiff(fileContent);
    const currentVersion = Math.max(
      ...Blockchain.getInstance()
        .blocks.reduce<Entry[]>(
          (accum, current) => (accum = accum.concat(current.entries)),
          []
        )
        .filter((entry) => entry.linkedFilePURL == purl)
        .map((e) => e.versionNo)
    );
    const newEntry = new Entry(userId, currentVersion + 1, diff, purl, userSig);
    if (filterEntries(new Array<Entry>(newEntry), userSig).length > 0) {
      addBlock(new Array<Entry>(newEntry), userId);
      DocPool.getInstance().editDoc(purl, fileContent);
      return { DATATYPE: "UPDATEDDOC", PURL: purl, CONTENT: fileContent };
    }
  }
  return false;
};

export { createOrEditDoc };
