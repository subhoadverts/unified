import { Block } from "../../models/transient/block";
import { Blockchain } from "../../models/transient/blockchain";

const visualizeBlockchain = (): {
  blockId: number;
  blockHash: string;
  verifier: number;
  entries: { purl: string; version: number; timestamp: number; hash: string }[];
}[] => {
  const blocks = Blockchain.getInstance().blocks;
  const completeDetails = blocks.map((x) => ({
    blockId: x.id,
    blockHash: x.blockHash,
    verifier: x.verifierId,
    entries: x.entries.map((e) => ({
      purl: e.linkedFilePURL,
      version: e.versionNo,
      timestamp: e.timestamp,
      hash: e.hash,
    })),
  }));
  return completeDetails;
};

export { visualizeBlockchain };
