import { Blockchain } from "../../models/transient/blockchain";
import { Differential } from "../../models/transient/differential";
import { Entry } from "../../models/transient/entry";
import { Provenance } from "../../models/transient/provenance";

const getProvenance = (purl: string, userId: number): Provenance => {
  const blockchain = Blockchain.getInstance();

  const matchingEntries: Array<Entry> = blockchain.blocks
    .reduce<Entry[]>(
      (accum, current) => (accum = accum.concat(current.entries)),
      []
    )
    .filter((entry) => entry.linkedFilePURL == purl)
    .sort((e1, e2) => e1.timestamp - e2.timestamp);

  const details: {
    version: number;
    userId: number;
    timestamp: number;
    sizeDiff: number;
    readableDiffMap: Array<{ index: number; value: number }>;
    hash: string;
  }[] = matchingEntries.map((entry) => ({
    version: entry.versionNo,
    userId: entry.userId,
    timestamp: entry.timestamp,
    sizeDiff: entry.differential.sizeDiff,
    readableDiffMap: entry.differential.readableDiffMap(),
    hash: entry.hash,
  }));

  return new Provenance(userId, details);
};

export { getProvenance };
