import { Differential } from "./differential";

class Doc {
  constructor(private readonly _pURL: string, private _fileContent: string) {}

  public get pURL() {
    return this._pURL;
  }

  public get fileContent() {
    return this._fileContent;
  }

  public set fileContent(newContent: string) {
    this._fileContent = newContent;
  }

  public applyDiff = (diff: Differential): Doc => {
    const newSize = this.fileContent.length + diff.sizeDiff;
    const diffMap = diff.diffMap;

    let newContent = this.fileContent;

    if (newSize > newContent.length) {
      newContent = newContent.padEnd(newSize);
    } else {
      newContent = newContent.substring(0, newSize);
    }

    const replaceAtIndex = (
      original: string,
      index: number,
      asciiDiff: number
    ): string => {
      if (index >= original.length) {
        throw new Error("Oops! Index out of range!");
      }

      return (
        original.substring(0, index) +
        String.fromCharCode(original.charCodeAt(index) + asciiDiff) +
        original.substring(index + 1)
      );

      return original;
    };

    diffMap.forEach(
      (val, index) => (newContent = replaceAtIndex(newContent, index, val))
    );

    return new Doc(this._pURL, newContent);
  };

  public calculateDiff = (newContent: string): Differential => {
    let currentContent = this._fileContent;
    const sizeDiff = newContent.length - this._fileContent.length;
    const diffMap = new Map<number, number>();

    if (newContent.length > currentContent.length) {
      currentContent = currentContent.padEnd(newContent.length);
    } else {
      currentContent = currentContent.substring(0, newContent.length);
    }

    for (let i = 0; i < newContent.length; i += 1) {
      const asciiDiff = newContent.charCodeAt(i) - currentContent.charCodeAt(i);
      if (asciiDiff !== 0) {
        diffMap.set(i, asciiDiff);
      }
    }

    return new Differential(diffMap, sizeDiff);
  };

  public static getBlankDoc = () => new Doc("", "");
}

export { Doc };
