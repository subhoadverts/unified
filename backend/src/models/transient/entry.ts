import { getSHA256Hash } from "./../../commands/common";
import { Differential } from "./differential";

class Entry {
  constructor(
    userId: number,
    versionNo: number,
    differential: Differential,
    linkedFilePURL: string,
    userSig: number
  ) {
    this._userId = userId;
    this._versionNo = versionNo;
    this._differential = differential;
    this._linkedFilePURL = linkedFilePURL;
    this._userSig = userSig;
    this._timeStamp = Date.now();
    this._hash = this.calculateHash();
  }

  private readonly _userId: number;
  private readonly _versionNo: number;
  private readonly _differential: Differential;
  private readonly _linkedFilePURL: string;
  private readonly _userSig: number;

  public get userId() {
    return this._userId;
  }

  public get versionNo() {
    return this._versionNo;
  }

  public get differential() {
    return this._differential;
  }

  public get linkedFilePURL() {
    return this._linkedFilePURL;
  }

  public get userSig() {
    return this._userSig;
  }

  private readonly _timeStamp: number;
  public get timestamp() {
    return this._timeStamp;
  }

  private readonly _hash: string;
  public get hash() {
    return this._hash;
  }

  private calculateHash = (): string => {
    let strToHash = "";
    this._differential.diffMap.forEach(
      (val, indx) =>
        (strToHash = strToHash.concat(val.toString() + indx.toString()))
    );
    strToHash = strToHash.concat(this._userId.toString());
    strToHash = strToHash.concat(this._linkedFilePURL);
    strToHash = strToHash.concat(this._versionNo.toString());
    strToHash = strToHash.concat(this._userSig.toString());
    strToHash = strToHash.concat(this._timeStamp.toString());
    return getSHA256Hash(strToHash);
  };
}

export { Entry };
