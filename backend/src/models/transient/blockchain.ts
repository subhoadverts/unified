import { Block } from "./block";
import { Entry } from "./entry";

class Blockchain {
  private static _instance: Blockchain;
  private constructor() {}
  public static getInstance(): Blockchain {
    if (!Blockchain._instance) {
      Blockchain._instance = new Blockchain();
    }
    return Blockchain._instance;
  }

  public reset() {
    this._blocks = new Array<Block>();
  }

  private _blocks: Array<Block> = new Array<Block>();
  public get blocks() {
    return this._blocks;
  }

  private nextBlockId = () => this._blocks.length + 1;

  public addBlock = (entries: Array<Entry>, verifierId: number) =>
    this._blocks.push(new Block(this.nextBlockId(), entries, verifierId));
}

export { Blockchain };
