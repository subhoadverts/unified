import { Doc } from "./doc";

class DocPool {
  private static _instance: DocPool;
  private constructor() {}
  public static getInstance(): DocPool {
    if (!DocPool._instance) {
      DocPool._instance = new DocPool();
    }
    return DocPool._instance;
  }

  private _docArray: Array<Doc> = new Array<Doc>();

  public pURLExists = (pURL: string): Boolean =>
    this._docArray.map((doc) => doc.pURL).includes(pURL);

  public getDocByPURL = (pURL: string): Doc => {
    const requestedDoc = this._docArray.find((doc) => doc.pURL == pURL);
    if (requestedDoc instanceof Doc) {
      return requestedDoc;
    } else {
      throw new Error("Requested pURL does not exist");
    }
  };

  public getDocPURLs = () => this._docArray.map((doc) => doc.pURL);

  public addDoc = (pURL: string, content: string) => {
    const newDoc = new Doc(pURL, content);
    this._docArray.push(newDoc);
  };

  public editDoc = (pURL: string, content: string) => {
    const currDoc = this._docArray.find((doc) => doc.pURL == pURL);

    if (currDoc instanceof Doc) {
      currDoc.fileContent = content;
      this._docArray = this._docArray.filter((doc) => doc.pURL != pURL);
      this._docArray.push(currDoc);
    }
  };
}

export { DocPool };
