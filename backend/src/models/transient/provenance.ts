import { Differential } from "./differential";

class Provenance {
  constructor(
    private readonly _creatorId: number,
    private readonly _details: Array<{
      version: number;
      userId: number;
      timestamp: number;
      sizeDiff: number;
      readableDiffMap: Array<{ index: number; value: number }>;
      hash: string;
    }>
  ) {
    this._createdAt = Date.now();
  }

  public get creatorId() {
    return this._creatorId;
  }

  public get details() {
    return this._details;
  }

  private readonly _createdAt: number;
  public get createdAt() {
    return this._createdAt;
  }
}

export { Provenance };
