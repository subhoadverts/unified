import {
  FastifyInstance,
  FastifyReply,
  FastifyRequest,
  RouteShorthandOptions,
} from "fastify";
import { getProvenance } from "../../commands/transient/getProvenance";

module.exports = function (
  server: FastifyInstance,
  opts: RouteShorthandOptions,
  done: Function
) {
  server.route({
    method: "GET",
    url: "/provenance",
    schema: {
      querystring: {
        type: "object",
        properties: {
          purl: { type: "string" },
          userid: { type: "integer" },
        },
      },
    },
    handler: async function (
      req: FastifyRequest<{ Querystring: { purl: string; userid: number } }>,
      res: FastifyReply
    ) {
      const purl = req.query.purl;
      const userid = req.query.userid;
      const provenance = getProvenance(purl, userid);

      return provenance;
    },
  });
  done();
};
