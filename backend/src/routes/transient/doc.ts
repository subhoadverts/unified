import {
  FastifyInstance,
  FastifyReply,
  FastifyRequest,
  RouteShorthandOptions,
} from "fastify";

import { createOrEditDoc } from "../../commands/transient/createOrEditDoc";
import { getDoc } from "../../commands/transient/getDoc";
import { listAllPURLs } from "../../commands/transient/listAllPURLs";
import { retrieveVersion } from "../../commands/transient/retrieveVersion";
import { getVersionsForDoc } from "../../commands/transient/getVersionsForDoc";
import { getPURLsAndContent } from "../../commands/transient/getPURLsAndContent";

module.exports = function (
  server: FastifyInstance,
  opts: RouteShorthandOptions,
  done: Function
) {
  server.route({
    method: "GET",
    url: "/docs",
    handler: async function (req, res) {
      return listAllPURLs();
    },
  });

  server.route({
    method: "GET",
    url: "/docsWithContent",
    handler: async function (req, res) {
      return getPURLsAndContent();
    },
  });

  server.route({
    method: "GET",
    url: "/doc/:id/availableversions",
    handler: async function (
      req: FastifyRequest<{
        Params: {
          id: string;
        };
      }>,
      res: FastifyReply
    ) {
      return getVersionsForDoc(req.params.id);
    },
  });

  server.route({
    method: "GET",
    url: "/doc/:id",
    handler: async function (
      req: FastifyRequest<{ Params: { id: string } }>,
      res
    ) {
      return getDoc(req.params.id);
    },
  });

  server.route({
    method: "GET",
    url: "/doc/:id/:version",
    handler: async function (
      req: FastifyRequest<{
        Params: {
          id: string;
          version: number;
        };
      }>,
      res: FastifyReply
    ) {
      return retrieveVersion(req.params.id, req.params.version);
    },
  });

  server.route({
    method: "POST",
    url: "/doc/:id",
    handler: async function (
      req: FastifyRequest<{
        Body: {
          filecontent: string;
          userid: number;
          usersig: number;
        };
        Params: { id: string };
      }>,
      res: FastifyReply
    ) {
      const result = createOrEditDoc(
        req.params.id,
        req.body.filecontent,
        req.body.userid,
        req.body.usersig
      );

      if (result != false) {
        if (
          result.DATATYPE == "UPDATEDDOC" ||
          result.DATATYPE == "CREATEDDOC"
        ) {
          server.websocketServer.clients.forEach((client) =>
            client.send(JSON.stringify(result))
          );
        }
      }
      return result;
    },
  });

  done();
};
