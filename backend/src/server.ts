import { fastify, FastifyRequest } from "fastify";
import fastifyWS from "fastify-websocket";
import fastifyStatic from "fastify-static";
import path from "path";

const server = fastify({ logger: true });

server.register(require("fastify-formbody"));
server.register(require("fastify-cors"));
server.register(fastifyWS, { options: { clientTracking: true } });
server.register(fastifyStatic, {
  root: path.join(__dirname, "dist"),
});

//#region routes
server.register(require("./routes/transient/provenance"));
server.register(require("./routes/transient/doc"));
server.register(require("./routes/transient/visualizeblockchain"));

server.get("/wschannel", { websocket: true }, function wsHandler(conn, req) {
  console.log("WS Connection started...");
});

server.get("/", async (req, res) => {
  res.sendFile("index.html");
});
//#endregion

const startServer = async () => {
  try {
    await server.listen(process.env.PORT || 3000, "0.0.0.0");
    server.log.info("Started server...");
  } catch (err) {
    server.log.error(err);
  }
};

(async () => await startServer())();
