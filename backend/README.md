### Blockchain Version Control System

How to use this thing:

- Ensure you have Node and NPM installed
- Open terminal and run `npm install` from project root
- Open ./src and run `ts-node ./server.ts`
- You also need to install Redis
