import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loggedIn: false,
    userid: "011",
    usersig: "929"
  },

  mutations: {
    login (state, {userid, usersig}) {
      state.loggedIn = true;
      state.userid = userid;
      state.usersig = usersig;
    },
    logout(state){
      state.loggedIn= false;
      state.userid = null;
      state.usersig = null;
    }
  },
  actions: {},
  modules: {}
});
