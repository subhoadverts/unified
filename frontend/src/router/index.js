import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import CreateDocument from "../views/CreateDocument.vue";
import ViewDocument from "../views/ViewDocument.vue";
import EditDocument from "../views/EditDocument.vue";
import ViewOlderVersions from "../views/ViewOlderVersions.vue";
import ViewAllDocuments from "../views/ViewAllDocuments.vue";
import SeekProvenance from "../views/SeekProvenance.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/docs",
    name: "Documents",
    component: ViewAllDocuments
  },
  {
    path:"/view/:id/versions",
    name: "View Older Versions",
    component: ViewOlderVersions
  },
  {
    path:"/view/:id",
    name: "View",
    component: ViewDocument,
  },
  {
    path:"/edit/:id",
    name: "Edit",
    component: EditDocument
  },
  {
    path: "/create",
    name: "Create",
    component: CreateDocument
  },
  {
    path: "/provenance/:id",
    name: "Seek Provenance",
    component : SeekProvenance
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
